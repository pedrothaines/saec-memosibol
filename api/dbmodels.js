var libpath = require('path');
var config = require(libpath.join(process.cwd() ,'config.js'));
var Sequelize = require('sequelize');
var bcrypt = require('bcrypt');

// create a sequelize instance with our local postgres database information.
var sequelize = new Sequelize(config.address.userbdpath,{logging: config.othervars.logbdmessages});


// setup User model and its fields.
const User = sequelize.define('user',{

    username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: true,
        validate: {
            isEmail: true
        }
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    flagScan:{
        type: Sequelize.INTEGER,
        allowNull: false
    },
    lastScaned:{
        type: Sequelize.DATE,
        allowNull: true
    },
    scoretotal:{
        type:Sequelize.INTEGER,
        allowNull: true
    }
},{
    hooks:{
        beforeCreate:  function(user){
            const salt = bcrypt.genSaltSync();
            user.password = bcrypt.hashSync(user.password, salt);
            user.scoretotal = 0;
        }
    }

});

User.prototype.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
}


const Positions = sequelize.define('positions',{

    long_i: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    lat_i: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    time_i: {
        type: Sequelize.DATE,
        allowNull: false
    },
    lat_f: {
        type: Sequelize.DOUBLE,
        allowNull: true
    },
    long_f: {
        type: Sequelize.DOUBLE,
        allowNull: true
    },
    time_f: {
        type: Sequelize.DATE,
        allowNull: true
    },
    score: {
        type: Sequelize.INTEGER,
        allowNull: true
    }
});

User.hasMany(Positions);

// create all the defined tables in the specified database.
sequelize.sync()
    .then(() => console.log('[!] Tabela de usuarios criada (se nao previamente definida)'))
    .catch(error => console.log('[X] Erro no BD de usuarios >', error));

// export User model for use in other files.

module.exports = { User,Positions }


