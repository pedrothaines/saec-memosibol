var express = require('express');
var app = express();
var libpath = require('path');
var bodyParser = require("body-parser");
var CronJob = require('cron').CronJob;
var config = require(libpath.join(process.cwd() ,'config.js'));
var port = process.env.PORT || config.address.serverport;
var server = config.address.serverip;
var sequelize = require('sequelize');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
var async = require("async");

const {User, Positions } = require('./dbmodels.js'); // Importa os modelos de DB

var http = require('http').Server(app);
var server = http.listen(port, server, function() {
    console.log('[!] Node Server Running on: ' + port);
});

const OK = 200;
const NOK = 401;

var totensQrCode = ["totem1","totem2","totem3","totem4","totem5"];


function parseTime(s) {
   var c = s.split(':');
   return parseInt(c[0]) * 60 * 60 + parseInt(c[1]) * 60 + parseInt(c[2]);
}


// var minScanInterval = 30 * 60; // em segundos
var minScanInterval = 5; // em segundos

new CronJob('* * * * * 0', function() { //todo domingo
	User.findAll({where:{flagScan:0}}).then(function(users){
		if(users){
			users.forEach((user) => {
				user.update({
					scoretotal: 0
				})
			});
		}
	});
}, null, true);

new CronJob('* * * * * *', function() {
	User.findAll({where:{flagScan:0}}).then(function(users){
		if(users){
			users.forEach((user) => {
				const now = new Date().toString().substr(16,8);
				//var usrLastScaned = "2019-08-15 00:49:28.091-03".substr(11,8);
				var usrLastScaned = user.lastScaned.toString();
				var usrLastScaned = usrLastScaned.substr(16,8);
				// console.log(now);
				// console.log(usrLastScaned)
				var diffTime = parseTime(now) - parseTime(usrLastScaned);
				// console.log(diffTime);
				if(diffTime > minScanInterval){
					user.update({
						flagScan: 1
					});
				}
			});
		}else{

		}
	});
}, null, true);
//----------------------------------------------------------------//

function calculaScore(lat1,lat2,long1,long2){

	const MAX = 250;
	const rate = 1.2;

	var dist = calculaDistCoord(lat1,lat2,long1,long2);

	if(Math.pow(dist,rate) > MAX){
		return MAX;
	}else{
		return 1 + Math.round(Math.pow(dist,rate));
	}
}

//-----------------------------------------------------------------//
function calculaDistCoord(lat1,lat2,lon1,lon2){ // Fonte: https://www.movable-type.co.uk/scripts/latlong.html
	var R = 6371e3; // metres
	var φ1 = lat1 * Math.PI / 180;
	var φ2 = lat2 * Math.PI / 180;
	var Δφ = (lat2-lat1) * Math.PI / 180;
	var Δλ = (lon2-lon1) * Math.PI / 180;

	var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
	Math.cos(φ1) * Math.cos(φ2) *
	Math.sin(Δλ/2) * Math.sin(Δλ/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

	var d = R * c;
	console.log("DIST " + d)
	return d;
}

app.route('/signup')
.get((req, res) => {

	res.sendFile(libpath.join(process.cwd(),'public','signup.html'));
})
.post((req, res) => {
    var username = req.body.username
    User.findOne({ where: { username: username } }).then(function (user) {
        if (!user) {
           User.create({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            flagScan: 1
        })
           .then(user => {
            //req.session.user = user.dataValues;
            console.log("[!] Usuario criado! > " + req.body.username)
            res.redirect('/signup');
        })
           .catch(error => {
            console.log(error);
            res.redirect('/signup');
        }); 

       }else {
        console.log("[X] Usuario ja existe!");
        res.redirect("/signup");
       }
   })
});


app.post('/flagscan', function(req,res){
	if(req.body.user){
		User.findOne({where:{username:req.body.user}}).then(function(user){
			if(user){
				res.send({status:true, flagscan: user.flagScan})
			}else{
				res.send({status:false, msg:"user nao existe"})
			}
		})
	}
})

app.get('/hello', function(req,res){
	console.log(req.body)
	res.send({hello: "Hello json"});
});

app.get('/getranking', async function(req,res){
	var rank = []
	await User.findAll().then(async function(users){
		await users.forEach( (user) => {
			    rank.push({
				username: user.username,
				score: user.scoretotal
			})
		})
	})
	rank = rank.sort(function(a, b) {
    	return parseFloat(b.score) - parseFloat(a.score);
	});
	console.log({status:true , rank: rank})
	res.send({status:true , rank: rank})
})

app.post('/scantotem', function(req, res){
	if(req.body.user && req.body.latitude_f && req.body.longitude_f && req.body.qrcode && totensQrCode.includes(req.body.qrcode)){ //Verifica user e pos_f
		var username = req.body.user;
		var lat_f = parseFloat(req.body.latitude_f);
		var long_f = parseFloat(req.body.longitude_f);
		var qrCode = req.body.qrcode;

		User.findOne({where: {username: username}}).then(function(user){
			if(!user){
				res.send({status:false, msg: "username errado ou flagScan == 0"});
			}else{
				Positions.findOne({where:{userId: user.id, score: null}}).then(function(posLin){
					if(posLin){
						var thisscore = calculaScore(posLin.lat_i, lat_f, posLin.long_i, long_f)
						
						posLin.update({
							lat_f: lat_f,
							long_f: long_f,
							time_f: new Date(),
							score: thisscore
						}).then(p=>{
							user.update({
								lastScaned: new Date(),
								flagScan: 0,
								scoretotal: user.scoretotal + thisscore
							}).then(p=>{
								res.send({status:true, score:thisscore});
							})
						})

					}else{
						res.send({status:false, msg: "Precisa checktotem primeiro -- nenhuma linha em aberto"});
					}
					
				})

			}
		})
	}else{
		console.log(req.body.qrcode + "invalido")
		res.send({status: false, msg: "Faltam parametros"});
	}
})

app.post('/checktotem', function(req, res){
	var username = req.body.user;
	if(req.body.user && req.body.latitude_i && req.body.longitude_i){ //Verifica user e pos_i
		User.findOne({where:{username: username , flagScan: 1}}).then(function(user){	
			if(!user){
				res.send({status:false, msg: "username errado ou flagScan == 0"});
			}else{
				Positions.findOne({where:{userId: user.id , score: null}}).then(function(posLin){										
					if(posLin){	
						res.send({status: false, msg : "Notificacao pendente, escaneie o totem"})

					}else{
						Positions.create({
							lat_i: parseFloat(req.body.latitude_i),
							long_i: parseFloat(req.body.longitude_i),
							time_i : new Date(),
							userId : user.id
						}).then(posLin => {
							console.log("[!] notify enviada para o user " + username)
							res.send({status:true});
						}).catch(error => {
							console.log(error);
							res.send({status: false, msg: error});
						})
					}
				})
			}														 

		})
	}else{
		res.send("[X] Dados necessários não recebidos. Por favor envie -user- e -pos_i-");
	}
})

app.route('/login')
    .get(function(req, res){
        res.send("Rota Login");
    })
    .post(function(req, res){
        
        var username = req.body.user,
            password = req.body.password;

        User.findOne({ where: { username: username } }).then(function (user) {
            if (!user) {
                console.log("[X] Usuario " + username  + " nao existe")
                
                res.send("user nao cadastrado - visite /signup milho");
                
            } else if (!user.validPassword(password)) {
                console.log("[X] Senha incorreta para usuario " + username)
               
                res.sendStatus(NOK);
                
            } else {
            	console.log("[!] User " + username + "logado!");
            	res.sendStatus(OK);
            }
        })
        .catch(error => {
            console.log(error);
        });
});
