
// Arquivo de configuração do servidor
var config = {};

config.address = {};
config.othervars = {};


//config.address.serverip = "localhost"; //Mudar localhost pra ip da maquina servidora
config.address.serverip = "10.0.0.100"
config.address.serverport = 8081



//// USER DB VARS


config.othervars.logbdmessages = false; // logar mensagens sql do bd de usuarios

config.address.userbd = 5432 
config.address.userbdname = "SAECHACKTHON";
config.address.userbdip = "localhost";
config.address.userbdpath = "postgres://postgres@"+config.address.userbdip+":"+config.address.userbd+"/"+config.address.userbdname


module.exports = config;
