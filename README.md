# Tema do SAECKATHON

```
Prevenção ao sedentarismo e incentivo à atividade física
```
<br/>
![alt text](media/images/sedentarismo_homer.png "Homer Simpson")

**Fonte (imagem):** http://www.primeassessoriaesportiva.com.br/artigos/o-que-o-sedentarismo-faz/

## Equipe Memosibol
![alt text](media/images/manolima.png "Mano Lima")
* [Pedro Thaines](https://www.linkedin.com/in/pedrothaines/)
* [Rauan Pires](https://www.linkedin.com/in/rauan-pires-653a28181/)

## Projeto Ponto Saudável!

```
Incentivar a caminhada em empresas e instituições através de "gameficação" e sistema de recompensas!
```




