package com.example.saeckathon

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.android.volley.Response
import com.android.volley.toolbox.Volley
import com.android.volley.Request
import android.util.Log
import com.android.volley.toolbox.JsonObjectRequest

import org.json.JSONObject
import com.google.android.gms.location.LocationServices
import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest

import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.maps.model.LatLng
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import java.util.*
import kotlin.collections.HashMap

import kotlin.concurrent.schedule
import kotlin.concurrent.scheduleAtFixedRate

const val EXTRA_MESSAGE = "com.example.saeckathon.MESSAGE"

/*
    Referência para uso do GPS: https://en.proft.me/2019/01/3/how-get-location-latitude-longitude-android-kotlin/
 */

class MainActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    //var fusedLocationClient: FusedLocationProviderClient? = null

    var QR_CODE_RESULT = "totem1"

    var mGoogleApiClient: GoogleApiClient? = null
    var mLocation: Location? = null
    var mLocationManager: LocationManager? = null
    var mLocationRequest: LocationRequest? = null
    val listener: com.google.android.gms.location.LocationListener?= null

    val UPDATE_INTERVAL = (2 * 1000).toLong()  /* 2 secs */
    val FASTEST_INTERVAL: Long = 1000 /* 1 sec */

    var locationManager: LocationManager? = null

    private val isLocationEnabled: Boolean
        get() {
            locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            return locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*fusedLocationClient = LocationServices.
            getFusedLocationProviderClient(this)

        Log.d("TAG", fusedLocationClient.toString())*/

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        mLocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        Log.d("gggg","uooo");
        checkLocation()

        //val integrator = IntentIntegrator(this)
        //integrator.setOrientationLocked(true)

        //btnScanTotem.setOnClickListener {
        //    run {
        //        integrator.initiateScan()
        //    }
        //}

        //Timer("SettingUp", false).scheduleAtFixedRate(5000, 5000){
        //    Log.d("TIMER", "Working...")
        //    checkTotem()
        //}
    }


    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            if(ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.d("TAG", "FINE LOCATION GRANTED!")

                val reqSetting = LocationRequest.create().apply {
                    fastestInterval = FASTEST_INTERVAL
                    interval = UPDATE_INTERVAL
                    priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                    smallestDisplacement = 1.0f
                }


            } else {
                Log.d("TAG", "FINE LOCATION NOT GRANTED!")
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS)
                Log.d("TAG", MY_PERMISSIONS.toString())
            }
            return
        }

        startLocationUpdates()

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)

        if (mLocation == null) {
            startLocationUpdates()
        }
        if (mLocation != null) {

            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onConnectionSuspended(i: Int) {
        Log.i("TAG", "Connection Suspended")
        mGoogleApiClient!!.connect()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.i("TAG", "Connection failed. Error: " + connectionResult.getErrorCode())
    }

    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.connect()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient!!.isConnected()) {
            mGoogleApiClient!!.disconnect()
        }
    }

    @SuppressLint("MissingPermission")
    protected fun startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(UPDATE_INTERVAL)
            .setFastestInterval(FASTEST_INTERVAL)
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
            mLocationRequest, this)
        Log.d("reque", "--->>>>")
    }

    override fun onLocationChanged(location: Location) {

        val msg = "Updated Location: " +
                java.lang.Double.toString(location.latitude) + "," +
                java.lang.Double.toString(location.longitude)
        //mLatitudeTextView!!.text = location.latitude.toString()
        //mLongitudeTextView!!.text = location.longitude.toString()
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        // You can now create a LatLng Object for use with maps
        val latLng = LatLng(location.latitude, location.longitude)
    }
    fun checkLocation(): Boolean {
        if (!isLocationEnabled)
            Log.d("TAG", "Location is not enabled!")
            //showAlert()
        return isLocationEnabled
    }

    var MY_PERMISSIONS = 0

    fun checkTotem(view: View) {
        val userEditText = findViewById<EditText>(R.id.usernameInput).text
        //val passwordEditText = findViewById<EditText>(R.id.passwordInput).text
        val httpResponse = findViewById<TextView>(R.id.httpResponse)

        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.d("TAG", "FINE LOCATION GRANTED!")

            val reqSetting = LocationRequest.create().apply {
                fastestInterval = FASTEST_INTERVAL
                interval = UPDATE_INTERVAL
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                smallestDisplacement = 1.0f
            }


        } else {
            Log.d("TAG", "FINE LOCATION NOT GRANTED!")
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS)
            Log.d("TAG", MY_PERMISSIONS.toString())
        }

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        Log.d("TAG", mLocation.toString())

        val queue = Volley.newRequestQueue(this)
        val url = "http://10.0.0.100:8081/checktotem"

        Log.d("TAG", url)

        var mLatitude = mLocation?.latitude
        var mLongitude = mLocation?.longitude

        val params = HashMap<String, String> ()
        params["user"] = userEditText.toString()
        params["latitude_i"] = mLatitude.toString()
        params["longitude_i"] = mLongitude.toString()

        val jsonObject = JSONObject(params)

        Log.d("TAG", jsonObject.toString())


        if(params["latitude_i"] != "null" || params["longitude_i"] != "null") {
            Log.d("TAG", "Latitude e Latitude OK!")

            val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, url, jsonObject,
                Response.Listener { response ->
                    httpResponse.text = "Response: %s".format(response.toString())
                    //Log.d("TAG", response.getJSONObject("score").toString())
                },
                Response.ErrorListener {
                    httpResponse.text = "Network error!"
                    Log.d("TAG", it.toString())
                })

            queue.add(jsonObjectRequest)
        } else {
            Log.d("TAG", "Latitude ou Longitude NULL!")
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val qrCodeResult = findViewById<TextView>(R.id.qrCodeResult)
        var result: IntentResult? = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if(result != null){

            if(result.contents != null){
                qrCodeResult.text = result.contents
                QR_CODE_RESULT = result.contents.toString()
                Log.d("TAG", QR_CODE_RESULT)

            } else {
                qrCodeResult.text = "Scan failed!"
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun scanTotem(view: View) {
        val integrator = IntentIntegrator(this)
        integrator.setOrientationLocked(true)
        integrator.setPrompt("Escaneie seu totem para ganhar pontos!")
        //integrator.setTimeout(10000)

        integrator.initiateScan()

        val userEditText = findViewById<EditText>(R.id.usernameInput).text
        //val passwordEditText = findViewById<EditText>(R.id.passwordInput).text
        val httpResponse = findViewById<TextView>(R.id.httpResponse)

        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.d("TAG", "FINE LOCATION GRANTED!")

            val reqSetting = LocationRequest.create().apply {
                fastestInterval = FASTEST_INTERVAL
                interval = UPDATE_INTERVAL
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                smallestDisplacement = 1.0f
            }


        } else {
            Log.d("TAG", "FINE LOCATION NOT GRANTED!")
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS)
            Log.d("TAG", MY_PERMISSIONS.toString())
        }

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        Log.d("TAG", mLocation.toString())

        val queue = Volley.newRequestQueue(this)
        val url = "http://10.0.0.100:8081/scantotem"

        var mLatitude = mLocation?.latitude
        var mLongitude = mLocation?.longitude

        val params = HashMap<String, String> ()
        params["user"] = userEditText.toString()
        params["latitude_f"] = mLatitude.toString()
        params["longitude_f"] = mLongitude.toString()
        params["qrcode"] = QR_CODE_RESULT

        val jsonObject = JSONObject(params)

        Log.d("TAG", jsonObject.toString())


        Log.d("TAG", "Enviou!!! Totem nao eh empty!!")
        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, url, jsonObject,
            Response.Listener { response ->
                httpResponse.text = "Response: %s".format(response.toString())
                //Log.d("TAG", response.getJSONObject("score").toString())
            },
            Response.ErrorListener {
                httpResponse.text = "Network error!"
                Log.d("TAG", it.toString())
            })

        queue.add(jsonObjectRequest)
    }

    fun sendQRCodeResult(view: View) {
        val userEditText = findViewById<EditText>(R.id.usernameInput).text
        val passwordEditText = findViewById<EditText>(R.id.passwordInput).text
        val textView = findViewById<TextView>(R.id.httpResponse)

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            Log.d("TAG", "CAMERA GRANTED!")
        } else {
            Log.d("TAG", "CAMERA NOT GRANTED!")
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), MY_PERMISSIONS)
            Log.d("TAG", MY_PERMISSIONS.toString())
        }

        val intent = Intent(this@MainActivity, ScanActivity::class.java)
        startActivity(intent)
    }

    fun sendLoginReq(view: View) {
        /* Send HTTP POST to http://server.ip:server_port/login
           Body must be a JSON
           JSON DATA:
           user:
           password:
        */

        val userEditText = findViewById<EditText>(R.id.usernameInput).text
        val passwordEditText = findViewById<EditText>(R.id.passwordInput).text
        //val message = "Usuário $userEditText e $passwordEditText enviados!"

        val textView = findViewById<TextView>(R.id.httpResponse)

        val queue = Volley.newRequestQueue(this)
        val url = "http://10.0.0.100:8081/login"

        Log.d("TAG", url)



        /*
        val stringRequest = StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->
                try {
                    textView.text = "Response: $response"

                    val intent = Intent(this, DisplayMessageActivity::class.java).apply {
                        putExtra(EXTRA_MESSAGE, response)
                    }
                    Log.d("TAG", response)
                    startActivity(intent)

                } catch (e: Exception) {
                    textView.text = "Exception: $e"
                }
            },
            Response.ErrorListener { textView.text = "Error!" })

        queue.add(stringRequest)
        */

        val params = HashMap<String, String> ()
        params["user"] = userEditText.toString()
        params["password"] = passwordEditText.toString()

        val jsonObject = JSONObject(params)

        Log.d("TAG", jsonObject.toString())


        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, url, jsonObject,
            Response.Listener { response ->
                textView.text = "Response: %s".format(response.toString())
            },
            Response.ErrorListener {
                textView.text = "Network error!"
                Log.d("TAG", it.toString())
            })

        queue.add(jsonObjectRequest)


    }
}
